/* eslint-disable jsx-a11y/alt-text */
import {Component} from 'react';
import {connect} from 'dva';
import './drinks.css';
import Button from '../../components/categoryButton/categoryButton';
import DrinkData from '../../components/drinksContent/drinksContent';
import images from '../../theme/Images';
import {Layout, Row, Col} from 'antd';
import _ from 'lodash';
const {Content} = Layout;

const mapStateToProps = state => {
  return {
    beverages: _.get(state, 'beverages.beverages', []),
    types: _.get(state, 'types.types', []),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_Beverages(loading) {
      dispatch({
        type: 'beverages/GET_Beverages',
        loading,
      });
    },
    GET_Types(loading) {
      dispatch({
        type: 'types/GET_Types',
        loading,
      });
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Coffee extends Component {
    state = {
      loading: false,
    };
    componentDidMount = () => {
      const {GET_Beverages, GET_Types} = this.props;
      const loading = bool => this.setState({loading: bool});
      GET_Beverages(loading);
      GET_Types(loading);
    };

    render() {
      const {data} = this.props.beverages;
      const typesData = this.props.types.data;
      console.log(_.filter(typesData, ['category', 1]));
      const {loading} = this.state;
      return (
        <Layout className="wrapper">
          <div>
            <img src={images.drinksBanner} className="drinksBanner"></img>
          </div>
          <Content>
            <Row>
              {_.filter(typesData, ['category', 1]).map((item, index) => (
                <Col>
                  <Button key={index} id={index} drinksCategory={item}></Button>
                </Col>
              ))}
            </Row>
            <div>
              <h2 className="drinksHeadline">茶瓦納</h2>
            </div>
            {_.isUndefined(data) ? (
              'Loading'
            ) : (
              <Row gutter={[16, 16]}>
                {_.uniqBy(data, o => o.beverageName).map((item, index) => (
                  <Col xs={{span: 24}} sm={{span: 12}} md={{span: 8}}>
                    <DrinkData key={index} id={index} beveragesData={item} />
                  </Col>
                ))}
              </Row>
            )}
          </Content>
        </Layout>
      );
    }
  },
);
