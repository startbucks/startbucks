import React, {Component} from 'react';
import Card from '../components/Card';
import {Images} from '../theme';
import {Link} from 'dva/router';
import {connect} from 'dva';
import _ from 'lodash';

/*
const newsData = [
  {
    img: Images.event1,
    title: 'Summer Stay Cool 飲料集點贈',
    date: '2021/08/29',
  },
  {
    img: Images.event2,
    title: '獨享禮遇JCB晶緻卡-生活好康【星巴克好友分享優惠】 ',
    date: '2021/12/31',
  },
  {
    img: Images.event3,
    title: '【行動預點】新服務，全台指定門市上線試營運',
    date: '2021/08/11',
  },
  {
    img: Images.event4,
    title: '【星活力旅程】訂閱方案截止銷售，請留意兌換期限',
    date: '2021/07/12',
  },
  {
    img: Images.event5,
    title: '【好友分享日】週一我們陪你一起上班',
    date: '2021/06/11',
  },
  {
    img: Images.event1,
    title: 'Summer Stay Cool 飲料集點贈',
    date: '2021/08/29',
  },
  {
    img: Images.event2,
    title: '獨享禮遇JCB晶緻卡-生活好康【星巴克好友分享優惠】 ',
    date: '2021/12/31',
  },
  {
    img: Images.event3,
    title: '【行動預點】新服務，全台指定門市上線試營運',
    date: '2021/08/11',
  },
  {
    img: Images.event4,
    title: '【星活力旅程】訂閱方案截止銷售，請留意兌換期限',
    date: '2021/07/12',
  },
  {
    img: Images.event5,
    title: '【好友分享日】週一我們陪你一起上班',
    date: '2021/06/11',
  },
];
*/
const mapStateToProps = state => {
  return {
    newsDataList: _.get(state, 'news.newsDataList', []),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_allNews() {
      dispatch({type: 'news/GET_allNews'});
    },
  };
};

const styles = {
  body: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  banner: {
    width: '100%',
    height: 'auto',
    position: 'relative',
  },
  bannerBottomImg: {
    position: 'absolute',
    left: '0px',
    bottom: '0px',
    height: '60px',
  },
  content: {
    width: '80%',
    marginTop: '30px',
    marginBottom: '50px',
  },
  title: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '15px',
    fontWeight: 'bold',
    fontSize: '24px',
    letterSpacing: '2px',
    paddingLeft: '5px',
  },
  greenBar: {
    marginLeft: '10px',
    width: '15%',
    height: '5px',
    backgroundColor: '#00643C',
  },
  newsBlock: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class news extends Component {
    constructor(props) {
      super(props);

      this.state = {
        loading: false,
      };
    }

    componentDidMount() {
      const {GET_allNews} = this.props;
      GET_allNews();
    }
    render() {
      const {newsDataList} = this.props;
      return (
        <div style={styles.body}>
          <div style={styles.banner}>
            <img src={Images.news_banner} alt="" />
            <img
              src={Images.news_bottom}
              alt=""
              style={styles.bannerBottomImg}
            />
          </div>

          <div style={styles.content}>
            <div style={styles.title}>
              最新活動
              <div style={styles.greenBar}></div>
            </div>

            <div style={styles.newsBlock}>
              {newsDataList.map((item, index) => (
                <Link to={{pathname: `/news/detail/id=${index + 1}`}}>
                  <Card key={index} id={index} newsDataList={item} />
                </Link>
              ))}
            </div>
          </div>
        </div>
      );
    }
  },
);
