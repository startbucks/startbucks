/* eslint-disable jsx-a11y/alt-text */
import {Component} from 'react';
import './companyIntroduction.css';
import {Layout, Row, Col} from 'antd';
import {Link} from 'react-router-dom';
import images from '../../theme/Images';
const {Content} = Layout;

export default class Company extends Component {
  render() {
    return (
      <Layout>
        <Content>
          <Row>
            <Col span={24}>
              <div>首頁 > 公司介紹</div>
            </Col>
          </Row>
          <Row className="content">
            <Col span={24} className="title">
              <h2 className="about">關於星巴克</h2>
            </Col>
            <Row>
              <Col xs={{span: 24}} sm={{span: 12}}>
                <Row>
                  <Col span={24} className="textBlock">
                    <img src={images.history}></img>
                    <h3 className="questionTitle">為甚麼取名叫星巴克呢？</h3>
                    <h4 className="answerText">
                      Starbuck
                      是小說《白鯨》裡的一個人物角色，一個愛喝咖啡的副船長。星巴克的創始人之一Bowker
                      是一個作家，他覺得以St
                      開頭的名字給人感覺充滿力量，於是他們以這個字母開頭想了一堆名字。
                      無意中有人在一張舊石礦地圖上看到一個小鎮叫Starbo，於是他們想到了Starbuck
                      這個人。
                    </h4>
                  </Col>
                  <Col span={24}>
                    <img src={images.intro2} className="intro2"></img>
                  </Col>
                  <Col span={24} className="enterpriseMission">
                    <h3>星巴克的企業使命</h3>
                    <h4>
                      啟發並滋潤人們的心靈，每個人、每一杯、每個社區中皆能體現。
                      秉持續追求卓越以及實踐企業使命與價值觀，
                      透過每一杯咖啡的傳遞，將獨特的星巴克體驗帶入每位顧客的生活中。
                    </h4>
                  </Col>
                  <Col span={24}>
                    <img src={images.intro4}></img>
                  </Col>
                </Row>
              </Col>
              <Col xs={{span: 24}} sm={{span: 12}}>
                <Row>
                  <Col span={24}>
                    <h1 className="bigMark">WHO</h1>
                    <h1 className="bigMark">ARE</h1>
                    <h1 className="bigMark">
                      WE？<hr className="line"></hr>
                    </h1>
                  </Col>
                  <Col span={24}>
                    <img src={images.intro1} className="intro1"></img>
                  </Col>
                  <Col span={24} className="ethicalSourcing">
                    <h3>星巴克的道德採購</h3>
                    <h4>
                      一杯咖啡的品質源於土壤。星巴克是獨一無二的，
                      因為從農場種植到你手中的飲品，我們都全程直接參與了咖啡製作的工程，
                      與種植者及其社區建立了長期的關係，以確保品質。
                    </h4>
                  </Col>
                  <Col span={24}>
                    <img src={images.intro3}></img>
                  </Col>
                  <Col span={24} className="slogan">
                    <h3>星巴克的標語口號</h3>
                    <h4>將每一粒咖啡豆的風味發揮盡致</h4>
                    <h1 className="sloganText">Slogan</h1>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Row>
        </Content>
      </Layout>
    );
  }
}
