/* eslint-disable jsx-a11y/alt-text */
import {Component} from 'react';
import {connect} from 'dva';
import {Row, Col, Layout} from 'antd';
import './drinksDetail.css';
import images from '../../theme/Images';
import _ from 'lodash';
const {Content} = Layout;

const mapStateToProps = state => {
  return {
    specificBeverage: _.get(state, 'specificBeverage.specificBeverage', []),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_specificBeverage(loading, id) {
      dispatch({
        type: 'specificBeverage/GET_specificBeverage',
        loading,
        id,
      });
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Detail extends Component {
    state = {
      loading: false,
      id: undefined,
    };
    componentDidMount = () => {
      const {GET_specificBeverage} = this.props;
      const loading = bool => this.setState({loading: bool});
      const {id} = this.props.match.params;
      GET_specificBeverage(loading, id);
      console.log(id);
    };
    static getDerivedStateFromProps(nextProps, prevState) {
      const nextId = nextProps.match.params.id;
      const previousId = prevState.id;
      const {GET_specificBeverage} = nextProps;
      if (!_.isEqual(nextId, previousId)) {
        GET_specificBeverage(null, nextId);
        return {
          id: nextId,
        };
      }
      return null;
    }
    render() {
      const {data} = this.props.specificBeverage;
      console.log(data);

      return (
        <Content>
          {_.isUndefined(data) ? (
            'Loading'
          ) : (
            <div>
              <Row gutter={[0, 16]}>
                <Col span={24}>
                  <div style={{margin: '0 30px'}}>
                    ★ {data.beverageName}
                    <nbsp> </nbsp>
                    {data.beverageEnglishName}
                  </div>
                </Col>
              </Row>
              <Row className="drinksBox">
                <Col
                  xs={{span: 24}}
                  sm={{span: 9, offset: 3}}
                  className="drinksBlock">
                  <img src={images.bgCircle} className="bgCircle"></img>
                  <img src={images.BrewedCoffee} className="detailImg"></img>
                </Col>

                <Col xs={{span: 24}} sm={{span: 9, push: 2}}>
                  <Row>
                    <Col span={24}>
                      <h1 className="drinksTitle">{data.beverageName}</h1>
                    </Col>
                    <Col span={24}>
                      <h2 className="engTitle">{data.beverageEnglishName}</h2>
                    </Col>
                    <Col span={24} className="drinksText">
                      <h4>{data.beverageDescription}</h4>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row className="drinksTable">
                <Col span={24}>
                  <Row>
                    <Col span={12}>尺寸</Col>
                    <Col span={12}>價錢</Col>
                  </Row>
                  <Row className="tableContent">
                    <Col span={12}>{data.size}</Col>
                    <Col span={12}>{data.beveragePrice}</Col>
                  </Row>
                </Col>
              </Row>
            </div>
          )}
        </Content>
      );
    }
  },
);
