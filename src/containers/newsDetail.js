import React, { Component } from 'react';
import { Link } from "dva/router";
import { Breadcrumb } from 'antd';
import { Images } from '../theme';
import { connect } from 'dva';
import _ from 'lodash';
/*
const newsInfo = {
    date: '2021/08/11~2021/09/30',
    title: '星巴克【行動預點】新服務，全台指定門市上線試營運',
    img: Images.big_event3,
    info: `星巴克【行動預點】(Mobile Order & Pay)為星禮程會員專屬全新服務，2021/08/11 12:00起將有150家門市導入試營運。星禮程會員可透過手機APP線上預點，輕鬆配合行程規劃、到店自取免排隊，並可同步累積星星，希望提供顧客更佳消費體驗。
            使用步驟：
            1)更新/下載星巴克APP v4.3版，登入星禮程會員
            2)點選【行動預點】功能，預先點購客製化飲品與餐點
            3)透過星禮程隨行卡線上結帳，消費累積星星
            4)直接於指定時間內至指定門市，以訂單取餐免排隊
            
            服務說明：
            1.行動預點一經付款成立訂單，線上無法取消/修改訂單內容及取餐門市，請務必確認正確再下單。相關使用注意事項請詳見常見問題。
            2.行動預點服務不參與門市現場行銷活動與門市贈星活動，但每消費滿35元皆可獲得1顆星星，未來也將規劃專屬活動，敬請期待。
            3.2021/8/11起150家試營運門市資訊，請至星巴克網站【門市查詢】 (最終以星巴克行動APP【行動預點】 顯示門市為準)。
            4.瞭解更多【行動預點】服務注意事項請見常見問題。`,
};
*/
const styles = {
    content: {
        width: document.documentElement.clientWidth <= 768 ? '85%' : '60%',
    },
    bread: {
        marginTop: '30px',
        marginBottom: '15px',
    },
    dateText: {
        fontSize: '14px',
        color: '#777777',
        letterSpacing: '1px',
    },
    title: {
        marginBottom: '8px',
        fontWeight: 'bold',
        fontSize: document.documentElement.clientWidth <= 768 ? '18px' : '24px',
        letterSpacing: '1px',
    },
    img: {
        width: '100%',
    },
    info: {
        marginTop: '20px',
        marginBottom: '50px',
        fontSize: '15px',
        lineHeight: '2',
    },
    btn: {
        width: '150px',
        height: '40px',
        backgroundColor: 'transparent',
        border: '2px solid #00643C',
        borderRadius: '5px',
        color: '#00643C',
        fontWeight: 'bold',
        fontSize: '18px',
        letterSpacing: '1px',
        cursor: 'pointer',
        boxShadow: '3px 3px 3px #E6E6E6',
    }
}
const mapStateToProps = state => {
    return {
        newsData: _.get(state, 'news.newsData', []),
    };
};

const mapDispatchToProps = dispatch => {
    return {
        GET_oneNews(id) {
        {/*dispatch({ type: `news/GET_oneNews(${id})` });*/}
            dispatch({ type: 'news/GET_oneNews',id });
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(
    class newsDetail extends Component {
        constructor(props) {
            super(props);

            this.state = {
                loading: false,
            };
        }

        componentDidMount() {
            const { GET_oneNews } = this.props;
            const { id } = this.props.match.params;
            GET_oneNews(id);
        }
        render() {
            const { newsData } = this.props;
            return (
                <div
                    style={{
                        width: '100%',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}>
                    <div style={styles.content}>
                        <Breadcrumb separator=">" style={styles.bread}>
                            <Breadcrumb.Item><Link to='/home'>首頁</Link></Breadcrumb.Item>
                            <Breadcrumb.Item><Link to='/news'>最新消息</Link></Breadcrumb.Item>
                        </Breadcrumb>
                        <div style={styles.dateText}>{newsData.activityStartDate} ~ {newsData.activityEndDate}</div>
                        <div style={styles.title}>{newsData.activityName}</div>
                        <div style={styles.img}><img src={Images.big_event3} alt="" /></div>
                        <div style={styles.info}>{newsData.activityContent}</div>
                        <div style={{ width: '100%', display: 'flex', justifyContent: 'center', marginBottom: '60px' }}>
                            <button style={styles.btn} onClick={() => this.props.history.push("/news")}>返回活動列表</button>
                        </div>
                    </div>
                </div>
            );
        }
    }
)