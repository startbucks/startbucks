/* eslint-disable jsx-a11y/alt-text */
import {Component} from 'react';
import {connect} from 'dva';
import './foods.css';
import FoodsButton from '../../components/categoryButton/foodsButton';
import FoodsData from '../../components/foodsContent/foodsContent';
import images from '../../theme/Images';
import {Layout, Row, Col} from 'antd';
import _ from 'lodash';
const {Content} = Layout;

const mapStateToProps = state => {
  return {
    foods: _.get(state, 'foods.foods', []),
    types: _.get(state, 'types.types', []),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_Foods(loading) {
      dispatch({
        type: 'foods/GET_Foods',
        loading,
      });
    },
    GET_Types(loading) {
      dispatch({
        type: 'types/GET_Types',
        loading,
      });
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Foods extends Component {
    state = {
      loading: false,
    };
    componentDidMount = () => {
      const {GET_Foods, GET_Types} = this.props;
      const loading = bool => this.setState({loading: bool});
      GET_Foods(loading);
      GET_Types(loading);
    };

    render() {
      const {data} = this.props.foods;
      const {loading} = this.state;
      const typesData = this.props.types.data;
      return (
        <Layout className="wrapper">
          <div>
            <img src={images.foodsBanner} className="foodBanner"></img>
          </div>
          <Content>
            <Row>
              {_.filter(typesData, ['category', 2]).map((item, index) => (
                <Col>
                  <FoodsButton
                    key={index}
                    id={index}
                    foodsCategory={item}></FoodsButton>
                </Col>
              ))}
            </Row>

            <div>
              <h2 className="drinksHeadline">麵包/點心</h2>
            </div>
            {_.isUndefined(data) ? (
              'Loading'
            ) : (
              <Row gutter={[16, 16]}>
                {data.map((item, index) => (
                  <Col xs={{span: 24}} sm={{span: 12}} md={{span: 8}}>
                    <FoodsData key={index} id={index} foodsData={item} />
                  </Col>
                ))}
              </Row>
            )}
          </Content>
        </Layout>
      );
    }
  },
);
