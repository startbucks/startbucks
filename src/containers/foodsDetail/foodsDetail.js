/* eslint-disable jsx-a11y/alt-text */
import {Component} from 'react';
import {Row, Col, Layout} from 'antd';
import './foodsDetail.css';
import images from '../../theme/Images';
import _ from 'lodash';
import {connect} from 'dva';
const {Content} = Layout;

const mapStateToProps = state => {
  return {
    specificFood: _.get(state, 'specificFood.specificFood', []),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_specificFood(loading, id) {
      dispatch({
        type: 'specificFood/GET_specificFood',
        loading,
        id,
      });
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class FoodsDetail extends Component {
    state = {
      loading: false,
      id: undefined,
    };
    componentDidMount = () => {
      const {GET_specificFood} = this.props;
      const loading = bool => this.setState({loading: bool});
      const {id} = this.props.match.params;
      GET_specificFood(loading, id);
    };
    static getDerivedStateFromProps(nextProps, prevState) {
      const nextId = nextProps.match.params.id;
      const previousId = prevState.id;
      const {GET_specificFood} = nextProps;
      if (!_.isEqual(nextId, previousId)) {
        GET_specificFood(null, nextId);
        return {
          id: nextId,
        };
      }
      return null;
    }

    state = {};
    render() {
      const {data} = this.props.specificFood;
      console.log(this.props.specificFood);
      return (
        <Layout>
          <Content>
            {_.isUndefined(data) ? (
              'Loading'
            ) : (
              <div>
                <Row gutter={[0, 16]}>
                  <Col span={24}>
                    <div>
                      ★ {data.foodName}
                      <nbsp> </nbsp>
                      {data.foodEnglishName}
                    </div>
                  </Col>
                </Row>
                <Row className="drinksBox">
                  <Col
                    xs={{span: 24}}
                    sm={{span: 9, offset: 3}}
                    className="drinksBlock">
                    <img src={images.bgCircle} className="bgCircle"></img>
                    <img src={images.GreekYogurt} className="detailImg"></img>
                  </Col>
                  <Col
                    xs={{span: 24}}
                    sm={{span: 9, push: 2}}
                    className="foodsBox">
                    <Row>
                      <Col span={24}>
                        <h1 className="foodsTitle">{data.foodName}</h1>
                      </Col>
                      <Col span={24}>
                        <h2 className="foodsEngTitle">
                          {data.foodEnglishName}
                        </h2>
                      </Col>
                      <Col span={24}>
                        <h4 className="foodPrice">${data.foodPrice}</h4>
                      </Col>
                      <Col span={24} className="foodsText">
                        <h4>{data.foodDescription}</h4>
                      </Col>
                      <Col span={24}>
                        <h4 className="foodsHighlight">{data.highlight}</h4>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
            )}
          </Content>
        </Layout>
      );
    }
  },
);
