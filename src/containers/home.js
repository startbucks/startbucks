import React, {Component} from 'react';
import {connect} from 'dva';
import _ from 'lodash';
import Slider from 'react-slick';
import Card from '../components/Card';
import {Row, Col} from 'antd';
import {Images} from '../theme';
import './home.css';
import {Link} from 'dva/router';

/*
const companyData = {
  info: '從原產地的一株咖啡樹，最終成為您手中的一杯咖啡；我們堅持採購並且烘焙最高品質的咖啡，這是我們一直努力的基本原則；最初的十英呎到最後十英呎的珍貴體驗，為咖啡的故事做了最佳的註解，同時也塑造出咖啡家族的獨特風味及口感特性，閱讀咖啡的故事，可以讓您更瞭解咖啡，豐富您的咖啡體驗。',
  slogan: '「 將每一粒咖啡豆的風味發揮盡致 」',
};


const newsData = [
  {
    img: Images.event1,
    title: 'Summer Stay Cool 飲料集點贈',
    date: '2021/08/29',
  },
  {
    img: Images.event2,
    title: '獨享禮遇JCB晶緻卡-生活好康【星巴克好友分享優惠】 ',
    date: '2021/12/31',
  },
  {
    img: Images.event3,
    title: '【行動預點】新服務，全台指定門市上線試營運',
    date: '2021/08/11',
  },
  {
    img: Images.event4,
    title: '【星活力旅程】訂閱方案截止銷售，請留意兌換期限',
    date: '2021/07/12',
  },
  {
    img: Images.event5,
    title: '【好友分享日】週一我們陪你一起上班',
    date: '2021/06/11',
  },
  {
    img: Images.event1,
    title: 'Summer Stay Cool 飲料集點贈',
    date: '2021/08/29',
  },
  {
    img: Images.event2,
    title: '獨享禮遇JCB晶緻卡-生活好康【星巴克好友分享優惠】 ',
    date: '2021/12/31',
  },
  {
    img: Images.event3,
    title: '【行動預點】新服務，全台指定門市上線試營運',
    date: '2021/08/11',
  },
];
*/
const mapStateToProps = state => {
  return {
    companyData: _.get(state, 'company.companyData', []),
    newsDataList: _.get(state, 'news.newsDataList', []),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_company() {
      dispatch({type: 'company/GET_company'});
    },
    GET_allNews() {
      dispatch({type: 'news/GET_allNews'});
    },
  };
};

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
};
const settings2 = {
  infinite: true,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  responsive: [
    {
      breakpoint: 1800,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 1400,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 1100,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 720,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
  nextArrow: <img src={Images.arrow_right} alt="" />,
  prevArrow: <img src={Images.arrow_left} alt="" />,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class home extends Component {
    constructor(props) {
      super(props);

      this.state = {
        loading: false,
      };
    }

    componentDidMount() {
      const {GET_company, GET_allNews} = this.props;
      GET_company();
      GET_allNews();
    }

    render() {
      const {companyData, newsDataList, history} = this.props;
      return (
        <div className="body">
          <div className="banner">
            <Slider {...settings}>
              <img src={Images.home_banner2} alt="" />
              <img src={Images.home_banner3} alt="" />
              <img src={Images.home_banner4} alt="" />
              <img src={Images.home_banner5} alt="" />
            </Slider>

            <img
              src={Images.banner_bottom}
              alt=""
              className="bannerBottomImg"
            />
          </div>

          <div className="grayBg">
            <Row>
              <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                <div
                  className="company"
                  style={{
                    marginLeft:
                      document.documentElement.clientWidth <= 995
                        ? '8%'
                        : document.documentElement.clientWidth <= 1400
                        ? '40%'
                        : '50%',
                  }}>
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      width: '100%',
                    }}>
                    <span className="titleText">公司介紹</span>
                    <div className="greenBar"></div>
                  </div>
                  <img src={Images.whoAreWe} alt="" className="img_whoAreWe" />
                </div>
              </Col>

              <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                <div className="companyInfo">
                  {companyData.description}
                  <button
                    className="btnMore"
                    onClick={() => this.props.history.push('/company')}>
                    了解更多
                  </button>
                </div>
              </Col>
            </Row>
          </div>

          <div style={{position: 'relative', width: '100%'}}>
            <div className="homeslogan">
              <p
                className="sloganText"
                style={{
                  fontSize:
                    document.documentElement.clientWidth <= 768
                      ? '18px'
                      : '22px',
                }}>
                {companyData.companySlogan}
              </p>
            </div>
            <img
              src={Images.coffee_beans}
              alt=""
              className="img_coffeeBeans"
              style={{
                display:
                  document.documentElement.clientWidth > 600 ? 'block' : 'none',
              }}
            />
          </div>

          <div className="news">
            <div className="newsTitle">
              最新活動
              <span className="grayTitle">Latest Events</span>
              <div className="newsGreenBar"></div>
            </div>
            <div className="newsBlock">
              <Slider {...settings2}>
                {newsDataList.map((item, index) => (
                  <Link to={{pathname: `/news/detail/id=${index + 1}`}}>
                    <Card key={index} id={index} newsDataList={item} />
                  </Link>
                ))}
              </Slider>
            </div>
          </div>
        </div>
      );
    }
  },
);
