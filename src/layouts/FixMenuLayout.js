import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'dva';
import {Menu} from 'antd';
import {
  MailOutlined,
  AppstoreOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import {Images} from '../theme';
import './FixMenuLayout.css';

const styles = {
  fill: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    flex: 1,
  },
  header: {
    width: '100%',
    height: '50px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#3A3A3A',
  },
  headerRight: {
    display: 'flex',
    paddingRight: '25px',
    margin: '0px',
  },
  headerIcon: {
    marginRight: '15px',
    overflow: 'hidden',
    cursor: 'pointer',
    display: document.documentElement.clientWidth <= 575 ? 'none' : 'block',
  },
  headerIconImg: {
    width: '22px',
  },
  content: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    overflowY: 'auto',
  },
  footer: {
    width: '100%',
    height: '50px',
    padding: '0px 20px',
    display: 'flex',
    justifyContent:
      document.documentElement.clientWidth <= 360 ? 'center' : 'space-between',
    alignItems: 'center',
    backgroundColor: '#3A3A3A',
  },
  footerText: {
    display: document.documentElement.clientWidth <= 360 ? 'none' : 'block',
    fontSize: '0.15rem',
    color: '#AFAFAF',
    letterSpacing: '1px',
    textAlign: 'center',
  },
};

class fixMenuLayout extends Component {
  render() {
    const {children} = this.props;
    const {history} = this.props;
    return (
      <div style={styles.fill}>
        <div style={styles.header}>
          <div className="headerLeft" onClick={() => history.push('/home')}>
            <div className="logo">
              <img src={Images.logo} />
            </div>
            <span className="starbucksText">STARBUCKS</span>
          </div>

          <ul style={styles.headerRight}>
            <li style={styles.headerIcon} onClick={() => history.push('/news')}>
              <img src={Images.white_logo} style={styles.headerIconImg} />
              <span className="headerText">最新活動</span>
            </li>
            <li
              style={styles.headerIcon}
              onClick={() => history.push('/drinks')}>
              <img src={Images.drink} style={styles.headerIconImg} />
              <span className="headerText">產品介紹</span>
            </li>
            <li style={styles.headerIcon} onClick={() => history.push('/food')}>
              <img src={Images.product} style={styles.headerIconImg} />
              <span className="headerText">商品介紹</span>
            </li>
            <li
              style={styles.headerIcon}
              onClick={() => history.push('/home')}
              style={{
                display:
                  document.documentElement.clientWidth <= 575
                    ? 'block'
                    : 'none',
                paddingRight: '5px',
              }}>
              <img src={Images.list} style={styles.headerIconImg} />
            </li>
          </ul>
        </div>

        <div style={styles.content}>{children}</div>

        <div style={styles.footer}>
          <span style={styles.footerText}>
            © 2020 Starbucks Coffee Company. All rights reserved.
          </span>
          <ul className="footerShare">
            <a
              href="https://www.facebook.com/starbuckstaiwan?ref=nf"
              target="_blank">
              <li className="footerIcon_fb">
                <img src={Images.fb} />
              </li>
            </a>
            <a href="https://www.instagram.com/starbuckstw/" target="_blank">
              <li className="footerIcon">
                <img src={Images.ig} />
              </li>
            </a>
            <a href="https://www.youtube.com/user/STARBUCKSTW" target="_blank">
              <li className="footerIcon">
                <img src={Images.yt} />
              </li>
            </a>
            <a
              href="https://line.me/R/ti/p/@471vnurh?from=page"
              target="_blank">
              <li className="footerIcon_line">
                <img src={Images.line} />
              </li>
            </a>
          </ul>
        </div>
      </div>
    );
  }
}
export default fixMenuLayout;
