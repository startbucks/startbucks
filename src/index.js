import dva from 'dva';
import 'antd/dist/antd.css';
import './index.css';
// 1. Initialize
const app = dva();

// 2. Plugins
// app.use({});

// 3. Model
// app.model(require('./models/appModel').default);
app.model(require('./models/beverages').default);
app.model(require('./models/foods').default);
app.model(require('./models/specificFood').default);
app.model(require('./models/specificBeverage').default);
app.model(require('./models/categoryTypes').default);
app.model(require('./models/companyModel').default);
app.model(require('./models/newsModel').default);

// 4. Router
app.router(require('./router').default);

// 5. Start
app.start('#root');
