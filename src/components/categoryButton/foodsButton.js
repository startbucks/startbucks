import React, {Component} from 'react';
import {Button} from 'antd';
import './categoryButton.css';
import {Link} from 'dva/router';

export default class FoodsButton extends Component {
  state = {
    name: [],
  };

  render() {
    const {foodsCategory} = this.props;
    return (
      <Link to="/cake">
        <Button>{foodsCategory.typeName}</Button>
      </Link>
    );
  }
}
