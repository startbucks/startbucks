import React, {Component} from 'react';
import {Link} from 'dva/router';
import {Button} from 'antd';
import './categoryButton.css';

export default class Category extends Component {
  state = {
    name: [],
  };

  render() {
    const {drinksCategory} = this.props;
    return (
      <div>
        <Link to="/coffee">
          <Button>{drinksCategory.typeName}</Button>
        </Link>
      </div>
    );
  }
}
