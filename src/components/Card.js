import React, {Component} from 'react';
import { Images } from '../theme';


const styles={
  card:{
    width:'250px',
    background: '#fff',
    margin:'.5rem ',
    boxShadow:'1px 1px 15px rgb(43 43 43 / 12%)',
    cursor: 'pointer'
   
  },
  img:{
    width:'250px',
    height:'250px',
    position: 'relative',
    overflow: 'hidden',
    display:'flex',
    justifyContent:'center',
  },
  img_news:{
    /*
    width:'100%',
    maxHeight: '280px',
    marginBottom: '5px',
    */
    position: 'absolute',
    top: '0',
    right: '0',
    left: '0',
    padding: '0',
    margin: 'auto',
    maxWidth: '100%',
    maxHeight: '100%',
  },
  title:{
    height:'40px',
    padding:'10px',
    fontSize: '14px',
    color:'#121212',
    textAlign:'center',
    fontWeight: 'bold',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  date:{
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    paddingBottom:'15px',
  },
  img_date:{
    width:'18px',
    marginRight:'8px',
  },
  dateText:{
    color: '#777777',
    fontSize: '12px',
    letterSpacing:'1px',
    
  }
}
export default class Card extends Component {
  render() {
    const {id, newsDataList} = this.props;
    return (
      <div id={id} style={styles.card}>
        <div style={styles.img}><img src={Images.event3} alt="" style={styles.img_news}/></div>
        <div style={styles.title}>{newsDataList.activityName}</div>
        <div style={styles.date}>
          <div style={styles.img_date}><img src={Images.datePicker} alt=""/></div>
          <span style={styles.dateText}>{newsDataList.activityStartDate}</span>
        </div>
      </div>
    );
  }
}
