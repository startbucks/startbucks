/* eslint-disable jsx-a11y/alt-text */
import {Component} from 'react';
import {Link} from 'dva/router';
import images from '../../theme/Images';
import './foodsContent.css';

export default class Foods extends Component {
  state = {};
  render() {
    const {foodsData} = this.props;
    console.log(this.props);
    return (
      <div>
        <Link to={`/food/detail/${foodsData.foodId}`}>
          <img src={images.GreekYogurt} className="foodsImg"></img>
        </Link>
        <h3 className="foodsContents">
          {foodsData.foodName}
          <br />
          {foodsData.foodEnglishName}
        </h3>
      </div>
    );
  }
}
