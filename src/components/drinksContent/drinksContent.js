/* eslint-disable jsx-a11y/alt-text */
import {Component} from 'react';
import './drinksContent.css';
import {Link} from 'dva/router';
import images from '../../theme/Images';

export default class Drinks extends Component {
  render() {
    const {beveragesData} = this.props;
    return (
      <div>
        <Link to={`/detail/${beveragesData.beverageId}`}>
          <img src={images.BrewedCoffee} className="drinksImg"></img>
        </Link>
        <h3 className="drinksContents">
          {beveragesData.beverageName}
          <br />
          {beveragesData.beverageEnglishName}
        </h3>
      </div>
    );
  }
}
