import { Component } from 'react';
import { connect } from 'dva';
import _ from 'lodash';

const mapStateToProps = state => {
  return {
    data: _.get(state, 'app.data', undefined),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GET_function() {
      dispatch({ type: 'app/GET_function' });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class App extends Component {
    constructor(props) {
      super(props);

      this.state = {
        loading: false,
      };
    }

    componentDidMount() {
      const { GET_function } = this.props;
      GET_function();
    }

    render() {
      const { data } = this.props;
      console.log(data);
      return (
        <div>

        </div>
      );
    }
  },
);
