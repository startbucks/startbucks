import React, {Component} from 'react';
import { Route, Switch, routerRedux, withRouter,Redirect } from "dva/router";
import Drinks from './containers/drinks/drinks';
import Detail from './containers/drinksDetail/drinksDetail';
import Company from './containers/companyIntroduction/companyIntroduction';
import {BrowserRouter as Router} from 'react-router-dom';
import Foods from './containers/foods/foods';
import FoodsDetail from './containers/foodsDetail/foodsDetail';
import Cakes from './containers/foods/cake';
import Coffee from './containers/drinks/coffee';
import home from './containers/home';
import news from './containers/news';
import newsDetail from './containers/newsDetail';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import 'antd/dist/antd.css';
import './index.css';
import FixMenuLayout from "./layouts/FixMenuLayout";

const {ConnectedRouter} = routerRedux;

class Root extends Component {
  render() {
    const {children} = this.props;
    return children;
  }
}

const RouterRoot = withRouter(Root);

// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  return (
    <ConnectedRouter history={props.history}>
      <RouterRoot {...props}>
        <FixMenuLayout {...props}>
          <Switch>
            <Route path="/home" exact component={home} />
            <Route path="/news" exact component={news} />
            <Route path="/news/detail" exact component={newsDetail} />
            <Route path="/news/detail/id=:id" exact component={newsDetail} />
            <Route path="/cake" exact component={Cakes} />
            <Route path="/food" exact component={Foods} />
            <Route path="/food/detail/:id" exact component={FoodsDetail} />
            <Route path="/company" exact component={Company} />
            <Route path="/detail/:id" exact component={Detail} />
            <Route path="/coffee" exact component={Coffee} />
            <Route path="/drinks" exact component={Drinks} />
            <Redirect from="/" to="/home" />
          </Switch>
        </FixMenuLayout>
      </RouterRoot>
    </ConnectedRouter>
  );
};
