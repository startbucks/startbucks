// eslint-disable-next-line import/no-anonymous-default-export
export default {
    //layout
    logo: require('../assets/logo.png').default,
    white_logo: require('../assets/white_logo.png').default,
    drink: require('../assets/drink.png').default,
    product: require('../assets/product.png').default,
    list: require('../assets/list.png').default,

    fb: require('../assets/fb.png').default,
    ig: require('../assets/ig.png').default,
    yt: require('../assets/yt.png').default,
    line: require('../assets/line.png').default,
    //homePage
    home_banner: require('../assets/home_banner.png').default,
    home_banner2: require('../assets/home_banner2.jpg').default,
    home_banner3: require('../assets/home_banner3.png').default,
    home_banner4: require('../assets/home_banner4.png').default,
    home_banner5: require('../assets/home_banner5.png').default,
    banner_bottom: require('../assets/banner_bottom.png').default,
    whoAreWe: require('../assets/whoAreWe.png').default,
    coffee_beans: require('../assets/coffee_beans.png').default,
    arrow_left: require('../assets/arrow_left.png').default,
    arrow_right: require('../assets/arrow_right.png').default,

    //news
    news_banner: require('../assets/news_banner.jpg').default,
    gray_bg: require('../assets/gray_bg.png').default,
    news_bottom: require('../assets/news_bottom.png').default,
    event1: require('../assets/event1.png').default,
    event2: require('../assets/event2.png').default,
    event3: require('../assets/event3.png').default,
    event4: require('../assets/event4.png').default,
    event5: require('../assets/event5.jpg').default,

    //newsDetail
    big_event3: require('../assets/big_event3.jpg').default,

    //Card
    datePicker: require('../assets/datePicker.png').default,
    //Driinks
  drinksBanner: require('../assets/drinks/banner_drinks.jpg').default,

  //DrinksDetail
  PureMatchaLatte: require('../assets/drinks/drinksDetail/PureMatchaLatte.jpg')
    .default,
  bgCircle: require('../assets/materials/bgCircle.png').default,
  BrewedCoffee: require('../assets/drinks/day/Brewed Coffee.jpg').default,
  IcedBrewedCoffee: require('../assets/drinks/day/Iced Brewed Coffee.jpg')
    .default,

  //Company
  history: require('../assets/company/history.png').default,
  intro1: require('../assets/company/intro-1.jpg').default,
  intro2: require('../assets/company/intro-2.jpg').default,
  intro3: require('../assets/company/intro-3.jpg').default,
  intro4: require('../assets/company/intro-4.jpg').default,

  //DrinksContent
  kidshotChocolate: require('../assets/drinks/day/KidsHotChocolate.jpg')
    .default,

  //Foods
  foodsBanner: require('../assets/foods/foodBanner.jpg').default,

  //FoodsDetail
  GreekYogurt: require('../assets/foods/foodsDetail/GreekYogurt.jpg').default,


}
