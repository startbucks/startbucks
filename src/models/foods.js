/* eslint-disable import/no-anonymous-default-export */
import {GET_Foods} from '../services/foods';

export default {
  namespace: 'foods',
  state: {},
  effects: {
    *GET_Foods({loading}, {call, put}) {
      try {
        if (loading) loading(true);
        const response = yield call(GET_Foods);
        console.log(response);
        yield put({type: 'SAVE_foods', payload: response.data});
        if (loading) loading(false);
      } catch (err) {
        console.log(err);
      }
    },
  },
  reducers: {
    SAVE_foods(state, {payload}) {
      return {
        ...state,
        foods: payload,
      };
    },
  },
};
