/* eslint-disable import/no-anonymous-default-export */
import {GET_Beverages} from '../services/beverage';

export default {
  namespace: 'beverages',
  state: {},
  effects: {
    *GET_Beverages({loading}, {call, put}) {
      try {
        if (loading) loading(true);
        const response = yield call(GET_Beverages);
        console.log(response);
        yield put({type: 'SAVE_beverages', payload: response.data});
        if (loading) loading(false);
      } catch (err) {
        console.log(err);
      }
    },
  },
  reducers: {
    SAVE_beverages(state, {payload}) {
      return {
        ...state,
        beverages: payload,
      };
    },
  },
};
