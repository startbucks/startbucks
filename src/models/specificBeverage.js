/* eslint-disable import/no-anonymous-default-export */
import {GET_specificBeverage} from '../services/specificBeverage';

export default {
  namespace: 'specificBeverage',
  state: {},
  effects: {
    *GET_specificBeverage({loading, id}, {call, put}) {
      try {
        if (loading) loading(true);
        const response = yield call(GET_specificBeverage, id);
        console.log(response);
        yield put({type: 'SAVE_specificBeverage', payload: response.data});
        if (loading) loading(false);
      } catch (err) {
        console.log(err);
      }
    },
  },
  reducers: {
    SAVE_specificBeverage(state, {payload}) {
      return {
        ...state,
        specificBeverage: payload,
      };
    },
  },
};
