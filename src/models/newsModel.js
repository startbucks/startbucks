import {GET_allNews,GET_oneNews} from '../services/newsService';
import moment from 'moment';

export default {
  namespace: 'news',
  state: {},
  effects: {
    *GET_allNews({}, {call, put}) {
      try {
        const response = yield call(GET_allNews);

        for(let i=0; i<response.data.data.length ;i++){
          let startDate=response.data.data[i].activityStartDate;
          response.data.data[i].activityStartDate=moment(startDate).format('YYYY-MM-DD');
        }

        yield put({type: 'SAVE_function', payload: response.data.data});

      } catch (err) {
        throw err;
      }
    },

    *GET_oneNews({id}, {call, put}) {
      try {
        const response = yield call(GET_oneNews,id);
        console.log("response=>",response);
        console.log("response.data.data=>",response.data.data);
        let startDate=response.data.data.activityStartDate;
        let endDate=response.data.data.activityEndDate;
        response.data.data.activityStartDate=moment(startDate).format('YYYY/MM/DD');
        response.data.data.activityEndDate=moment(endDate).format('YYYY/MM/DD');

        yield put({type: 'SAVE_function', payload2: response.data.data});
        console.log("payload2=>",response.data.data);
      } catch (err) {
        throw err;
      }
    },
  },
  reducers: {
    SAVE_function(state, {payload,payload2}) {
      return {
        ...state,
        newsDataList: payload,
        newsData: payload2,
      };
    },
  },
};
