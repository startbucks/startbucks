/* eslint-disable import/no-anonymous-default-export */
// import {GET_function} from '../services/appService';
import {GET_company} from '../services/companyService';

export default {
  namespace: 'company',
  state: {},
  effects: {
    *GET_company({}, {call, put}) {
      try {
        const response = yield call(GET_company);
        console.log('response.data=>', response.data);
        yield put({type: 'SAVE_function', payload: response.data.data});
      } catch (err) {
        throw err;
      }
    },
  },
  reducers: {
    SAVE_function(state, {payload}) {
      return {
        ...state,
        companyData: payload,
      };
    },
  },
};
