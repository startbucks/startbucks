/* eslint-disable import/no-anonymous-default-export */
import {GET_Types} from '../services/categoryTypes';

export default {
  namespace: 'types',
  state: {},
  effects: {
    *GET_Types({loading}, {call, put}) {
      try {
        if (loading) loading(true);
        const response = yield call(GET_Types);
        console.log(response);
        yield put({type: 'SAVE_types', payload: response.data});
        if (loading) loading(false);
      } catch (err) {
        console.log(err);
      }
    },
  },
  reducers: {
    SAVE_types(state, {payload}) {
      return {
        ...state,
        types: payload,
      };
    },
  },
};
