/* eslint-disable import/no-anonymous-default-export */
import {GET_specificFood} from '../services/specificFood';

export default {
  namespace: 'specificFood',
  state: {},
  effects: {
    *GET_specificFood({loading, id}, {call, put}) {
      try {
        if (loading) loading(true);
        const response = yield call(GET_specificFood, id);
        yield put({type: 'SAVE_specificFood', payload: response.data});
        if (loading) loading(false);
      } catch (err) {
        console.log(err);
      }
    },
  },
  reducers: {
    SAVE_specificFood(state, {payload}) {
      return {
        ...state,
        specificFood: payload,
      };
    },
  },
};
