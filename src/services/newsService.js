import axios from "axios";

const _axios = axios.create({
  baseURL: 'http://localhost:3000',
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImFkbWluSWQiOjEsImFjY291bnQiOiJhZG1pbjAxIn0sImV4cCI6MTYyOTk5ODE1OSwiaWF0IjoxNjI5OTk3MjU5fQ.blGc15Xb-X-VOtWa6tgG83wy12xqVeVVjeg5QPIpFxk'
  }
})

export function GET_allNews() {
  return _axios.get('/activities')
}

export function GET_oneNews(id,token) {
  console.log("id===>",id);
  return _axios.get(`/activities/${id}`)
  
}