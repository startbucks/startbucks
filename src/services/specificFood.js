import axios from 'axios';

const _axios = axios.create({
  baseURL: 'http://127.0.0.1:3000',
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

export function GET_specificFood(id) {
  return _axios.get(`/foods/onefood/${id}`);
}
