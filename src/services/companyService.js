import axios from "axios";

const _axios = axios.create({
  baseURL: 'http://localhost:3000',
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImFkbWluSWQiOjEsImFjY291bnQiOiJhZG1pbjAxIn0sImV4cCI6MTYyOTkxMjk4NSwiaWF0IjoxNjI5OTEyMDg1fQ.ok_VntBg63c1xKuxTqiW95LDZ762esXvAVWq-miJqKw'
  }
})

export function GET_company() {
  return _axios.get('/company')
}